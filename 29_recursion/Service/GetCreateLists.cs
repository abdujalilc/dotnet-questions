﻿using _29_recursion.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _29_recursion.Service
{
    internal class GetCreateLists
    {
        public static void Main()
        {
            List<IGroup> OriginalList;
            List<IGroup> HirarchList = new List<IGroup>();

            OriginalList = new List<IGroup>()
        {
            new Group() { ID = "g1", ParentID = null },
            new Group() { ID = "g2", ParentID = "g1" },
            new Group() { ID = "g3", ParentID = null },
            new Group() { ID = "g4", ParentID = "g3" },
            new Group() { ID = "g5", ParentID = "g4" },
            new Group() { ID = "g6", ParentID = "g5" } };

            HirarchList = GetCreateList(null, OriginalList);
        }
        static List<IGroup> GetCreateList(string id, List<IGroup> list)
        {
            List<IGroup> temp = new List<IGroup>();
            temp = (from item in list
                    where item.ParentID == id
                    select (IGroup)new Group(item.ID, item.Name, GetCreateList(item.ID, list))).ToList();

            return temp;
        }
    } 

}
