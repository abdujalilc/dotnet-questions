﻿public class Program2
{
    public static void Main()
    {
        testMethod();
    }

    static void testMethod()
    {
        ProcessBusinessLogic2 processBusinessLogic = new ProcessBusinessLogic2();
        processBusinessLogic.msDelegate += newMethod;
        processBusinessLogic.invokeMethod();//good

        processBusinessLogic.msDelegate.Invoke("asdf");//bad
    }

    private static int newMethod(string test)
    {
        return 1;
    }
}

public delegate int Message(string message);
public class ProcessBusinessLogic2
{
    public Message msDelegate;
    public event Message messageEvent;
    public void invokeMethod()
    {
        msDelegate.Invoke("qwr");
        messageEvent.Invoke("qwr");
    }
}
