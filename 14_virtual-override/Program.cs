﻿namespace _14_virtual_override
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Printer printer = new LaserPrinter();
            Console.WriteLine(printer.GetType());
            Console.WriteLine(printer.GetHashCode());

            Printer printer1 = new LaserPrinter();
            printer1.Install();

            Console.Read();
        }
    }
    class Printer
    {
        //now this works because not overrided
        public virtual void Install()
        {
            Console.WriteLine("Printer instaled");
        }
        public virtual void Print(string text)
        {
            Console.WriteLine("Printing: " + text);
        }
    }
    class LaserPrinter : Printer
    {
        public void Install()
        {
            Console.WriteLine("Laser printer installed successfully");
        }
        /* this is also not called
        public new void Install()
        {
            Console.WriteLine("Laser printer installed successfully");
        }*/
        /* this would work if overrided
        public override void Install()
        {
            Console.WriteLine("Laser printer installed successfully");
        }*/
    }
}