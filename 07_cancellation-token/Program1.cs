﻿public class Program1
{
    public static async Task Main(string[] args)
    {
        CancellationTokenSource? tokenSource = new CancellationTokenSource();

        CancellableMethod();

        while (true)
        {
            if (Console.ReadKey().KeyChar is 'c')
            {
                Console.WriteLine("\n" + "Thread is about to cancel");
                Thread.Sleep(3000);
                tokenSource.Cancel();
                //tokenSource = new CancellationTokenSource();
            }
        }

        void CancellableMethod()
        {
            Task task = Task.Run(async () =>
            {
                for (int i = 0; i < 100; ++i)
                {
                    Console.WriteLine(i + " Task.CurrentId: " + Task.CurrentId);
                    Thread.Sleep(1000);
                    if (tokenSource.IsCancellationRequested)
                        tokenSource.Token.ThrowIfCancellationRequested();
                }
            }, tokenSource.Token);
        }

        void test2()
        {
            (bool success, string html, int reqam) = test();
            if (success)
            {
                html = reqam.ToString();
            }
        }
        (bool, string, int) test()
        {
            return (true, "", 0);
        }

        static async Task<(bool, string)> TryGetHtml(string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return (false, null);
            }
            string html = await new HttpClient().GetStringAsync(url);
            return (true, html);
        }

    }

}

