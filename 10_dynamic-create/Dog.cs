﻿namespace DynamicCreate
{
    /// <summary>
    ///Dog
    /// </summary>
    public class Dog : IAnimal
    {
        /// <summary>
        ///Name
        /// </summary>
        private string _name { get; }

        /// <summary>
        ///Parameterized constructor
        /// </summary>
        ///< param name = "name" > dog name < / param >
        public Dog(string name)
        {
            _name = name;
        }

        /// <summary>
        ///Dog barking
        /// </summary>
        public void Cry()
        {
            Console.WriteLine($"{_name}Woof, woof");
        }
    }
}