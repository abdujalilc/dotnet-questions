﻿namespace DynamicCreate
{
    /// <summary>
    ///Cat
    /// </summary>
    public class Cat : IAnimal
    {
        /// <summary>
        ///Parameterless constructor
        /// </summary>
        public Cat()
        {

        }

        /// <summary>
        ///Cat miauing
        /// </summary>
        public void Cry()
        {
            Console.WriteLine("Miauw Miauw Miauw");
        }
    }
}