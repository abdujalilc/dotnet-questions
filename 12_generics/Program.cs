﻿namespace _12_generics
{
    internal class Program
    {
        static void Main(string[] args)
        {
            DataProcessor<string> dataProcessor = new DataProcessor<string>("Hello");
            dataProcessor.Process("Test");
            dataProcessor.Process<double>(3.25D);

            DataProcessor2<string> dataProcessor2 = new DataProcessor2<string>();
            dataProcessor2.Process(100);
            //dataProcessor2.Process("test");
            Console.ReadKey();

        }
    }
    class DataProcessor<T>
    {
        public DataProcessor(T value)
        {

        }
        public void Process<T>(T value)
        {
            Console.Write(value.GetType().Name + "\n ");
        }
    }

    class Processor<T>
    {
        public void Process(T value)
        {
            Console.WriteLine(value.GetType().Name);
        }
    }
    class DataProcessor2<U> : Processor<int>
    {

    }
}